
init: 
	cp .env.sample .env
	python3 -m venv env
	. ./env/bin/activate
	pip3 install -r requirements.txt

up:
	docker context use default
	docker-compose -f compose.authentik.yml up -d
	docker-compose -f compose.wekan.yml up -d

down:
	docker-compose -f compose.authentik.yml down -v
	docker-compose -f compose.wekan.yml down -v

run:
	./env/bin/uvicorn app.main:app --reload --host 0.0.0.0


test:
	./env/bin/pytest -s --sw app 

integration:
	./env/bin/uvicorn app.main:app --reload --host 0.0.0.0 

requirements:
	./env/bin/pip freeze -l > requirements.txt


.PHONY: init run test
