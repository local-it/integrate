# integrate

integrate all the api's

## Development

### Getting Started

```
make init
make up
# wait a moment for containers to start
make test
make run
make down
```


# notes

**Provider:** a leading system with userdata and a notification channel integrate can connect with (e.g. Authentik)  
**Apps:** Have API that integrate can interact with and e.g. can create user (e.g. Nextcloud, Wekan)  


## challenges



* How to handle errors in connected Apps

* Apps can have different unique constrains than the Provider
  e.g. Wekan requires a unique Mail addr and authentik doesn't
  the specifig module for the apps api needs to know how to handle these errors

* User in App could already exist
* 



https://pydantic-docs.helpmanual.io/
https://jsontopydantic.com/
https://pydantic-docs.helpmanual.io/datamodel_code_generator/
https://docs.python.org/3/library/unittest.mock.html