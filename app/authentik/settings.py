from pydantic import BaseSettings, Field

class AuthentikSettings(BaseSettings):
    baseurl: str = ""
    token: str = ""

    class Config:
        env_file = '.env'
        env_prefix = 'AUTHENTIK_'