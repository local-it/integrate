from __future__ import annotations

from typing import Any, Dict, List, Optional

from pydantic import BaseModel

from app.consumer.baseConsumer import BaseUser


class UsersObjItem(BaseModel):
    pk: int
    username: str
    name: str
    is_active: bool
    last_login: Optional[str]
    email: str
    avatar: str
    attributes: Dict[str, Any]
    uid: str

class GroupsObjItem(BaseModel):
    pk: str
    name: str
    is_superuser: bool
    parent: Optional[str]
    parent_name: Optional[str]
    users: List[int]
    attributes: Dict[str, Any]
    users_obj: List[UsersObjItem]

class AuthentikUser(BaseUser):
    pk: Optional[str]
    username: str
    name: str
    is_active: bool = True
    last_login: Optional[str] = None
    is_superuser: Optional[bool] = None
    groups: List[str] = []
    groups_obj: Optional[List[GroupsObjItem]] = None
    email: str
    avatar: Optional[str] = None
    attributes: Dict[str, Any] = {}
    uid: Optional[str] = None