import email
import logging

from app.authentik.settings import AuthentikSettings
from .api import Authentik
from .models import AuthentikUser
import pytest

@pytest.fixture()
def settings() -> AuthentikSettings:
    return AuthentikSettings(
        baseurl="http://localhost:9000/", token="foobar123",)

@pytest.fixture
def api(settings: AuthentikSettings) -> Authentik:
    return Authentik(settings)


def test_create_event_transport_already_exists(api: Authentik):
    exception = None
    try:
        api.create_event_transport("/","asd")
    except Exception as e:
        exception = e
    assert not exception == None # TODO create exception types


def test_create_event_rule_already_exists(api: Authentik):
    exception = None
    try:
        api.create_event_rule({"pk" : "asd"},"webhook")
    except Exception as e:
        exception = e
    assert not exception == None # TODO create exception types

def test_get_user_by_username(api: Authentik):
    u = AuthentikUser(username="akadmin",
             name="",
             groups=[],
             email="",
             is_active=True,
             attributes={}
             )
    u = api.get_user(u) 
    assert not u == None
    assert u.username == "akadmin"


def test_create_user(api: Authentik):
    u = AuthentikUser(username="banane",
             name="banane",
             groups=[],
             email="foo@example.org",
             is_active=True,
             attributes={}
             )
    if(api.get_user(u)):
        api.delete_user(api.get_user(u))

    assert api.get_user(u) == None

    c = api.create_user(u)
    assert u.username == c.username
    assert not c.pk == None

    assert api.delete_user(c) == True