import logging
from app import dependencies
from app.authentik.api import Authentik
from app.event_controller import Authentik_Hook_Model, EventController, Http_request
from fastapi import APIRouter, Depends, BackgroundTasks
from app.authentik.settings import AuthentikSettings

router = APIRouter()

ec = EventController(Authentik(AuthentikSettings()))

@router.post("/authentik/hook/")
async def hook(model: Authentik_Hook_Model,
               http_request: Http_request,
               background_tasks: BackgroundTasks
               ):
    logging.info(model)
    logging.info(http_request)
    if http_request.path == "/api/v3/core/users/":
        background_tasks.add_task(ec.handle_model_created_event, model)
    return 200

@router.get("/authentik/errors/")
async def errors():
    return ec._exceptions