from abc import ABC, abstractclassmethod, abstractproperty
from pydantic import BaseModel


class BaseUser(BaseModel):
    email: str
    name: str
    username: str

class BaseGroup(BaseModel):
    name: str


class Consumer(ABC):

    @abstractproperty
    def api(self):
        pass

    @abstractclassmethod
    def create_user(user: BaseUser) -> BaseUser: 
        pass

    @abstractclassmethod
    def create_group(group: BaseGroup) -> BaseGroup:
        pass