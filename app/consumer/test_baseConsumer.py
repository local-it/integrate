from .nextcloud.main import BaseUser, Consumer
from .wekan import main
from .nextcloud import main
from pytest_mock import MockerFixture
import pytest

@pytest.fixture
def testUser():
    return BaseUser(email="foo", id="asd", name="sd", username="sdfsfd")

def test_get_subclasses():
    l = []
    for sc in Consumer.__subclasses__():
        l.append(sc.__name__)
    assert "NextcloudConsumer" in l
    assert "WekanConsumer" in l