from unittest.mock import MagicMock
from app.consumer.baseConsumer import BaseGroup, BaseUser, Consumer

class Api:
    def create_user(self):
        pass

class NextcloudConsumer(Consumer):

    def __init__(self):
        self._api: Api = Api()

    @property
    def api(self):
        return self._api

    @api.setter
    def api(self, api):
        self._api = api

    def create_user(self, user: BaseUser): # Wekan):
        return self.api.create_user()

    def create_group(self, group: BaseGroup):
        print("Create Nextcloud Group: ", group)
        pass