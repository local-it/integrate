from pydantic import BaseSettings

class WekanSettings(BaseSettings):
    baseurl: str = ""
    user: str = ""
    password: str = ""
    
    class Config:
        env_file = '.env'
        env_prefix = 'WEKAN_'