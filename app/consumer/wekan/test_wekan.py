import requests
from app.consumer.wekan.models import User, UserBase
from app.consumer.wekan.settings import WekanSettings
from .api import WekanApi
import pytest

@pytest.fixture()
def settings() -> WekanSettings:
    return WekanSettings(baseurl="http://localhost:3000", user="api", password="foobar123")

@pytest.fixture
def api(settings: WekanSettings) -> WekanApi:
    r = requests.post("http://localhost:3000/users/register", json={"username": "api", "password": "foobar123", "email": "foo@example.org"})
    return WekanApi(settings)


def test_get_user(api: WekanApi):
    user = api.get_user("api")
    assert user.username == "api"
    assert type(user) == User

    user = api.get_user("doesnotexist")
    assert user == None

def test_get_users(api: WekanApi):
    assert True if "api"  in [u.username for u in api.get_all_users()] else False

def test_create_and_delete_user(api: WekanApi):
    user = api.create_user("foo", "foo42@bar.com", "")
    assert api.get_user("foo").username == "foo"
    assert type(user) is User
    api.delete_user(user.id)
    assert api.get_user("foo") == None
