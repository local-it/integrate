from pydantic import BaseModel, Field
from typing import Any, Dict, List, Optional

from pydantic import BaseModel

class Org(BaseModel):
    orgId: str
    orgDisplayName: str

class Team(BaseModel):
    teamId: str
    teamDisplayName: str

class Email(BaseModel):
    address: str
    verified: bool

class Notification(BaseModel):
    activity: str
    read: str

class Profile(BaseModel):
    avatarUrl: Optional[str]
    emailBuffer: Optional[List[str]]
    fullname: Optional[str]
    showDesktopDragHandles: Optional[bool]
    hideCheckedItems: Optional[bool]
    cardMaximized: Optional[bool]
    customFieldsGrid: Optional[bool]
    hiddenSystemMessages: Optional[bool]
    hiddenMinicardLabelText: Optional[bool]
    initials: Optional[str]
    invitedBoards: Optional[List[str]]
    language: Optional[str]
    moveAndCopyDialog: Optional[Dict[str, Any]]
    moveChecklistDialog: Optional[Dict[str, Any]]
    copyChecklistDialog: Optional[Dict[str, Any]]
    notifications: Optional[List[Notification]]
    showCardsCountAt: Optional[int]
    startDayOfWeek: Optional[int]
    starredBoards: Optional[List[str]]
    icode: Optional[str]
    boardView: str
    listSortBy: str
    templatesBoardId: str
    cardTemplatesSwimlaneId: str
    listTemplatesSwimlaneId: str
    boardTemplatesSwimlaneId: str

class SessionData(BaseModel):
    totalHits: Optional[int]

class Member(BaseModel):
    id: str = Field(..., alias="userId")
    isAdmin: bool = False
    isNoComments: bool = False
    isCommentOnly: bool = False
    isWorker: Optional[bool] = False

class UserBase(BaseModel):
    id: str = Field(..., alias='_id')
    username: Optional[str] = None

class User(UserBase):
    username: str
    orgs: Optional[List[Org]]
    teams: Optional[List[Team]]
    emails: List[Email]
    createdAt: str
    modifiedAt: str
    profile: Optional[Profile]
    services: Dict[str, Any]
    heartbeat: Optional[str]
    isAdmin: Optional[bool]
    createdThroughApi: Optional[bool]
    loginDisabled: Optional[bool]
    authenticationMethod: str
    sessionData: SessionData
    importUsernames: Optional[List[Optional[str]]]
