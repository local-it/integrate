
from unittest.mock import MagicMock
from .settings import WekanSettings
from app.consumer.baseConsumer import BaseGroup, BaseUser, Consumer
from .api import WekanApi
from .models import User


class WekanConsumer(Consumer):

    def __init__(self):
        self._settings = WekanSettings()
        self._api = WekanApi(self._settings)

    @property
    def api(self):
        return self._api

    @api.setter
    def api(self, api):
        self._api = api

    def create_user(self, user: BaseUser):
        if self._api.get_user(user.username) == None:
            return self._api.create_user(username=user.username, email=user.email, password="")
        raise Exception("[Wekan] User already exists")

    def create_group(self, group: BaseGroup):
        print("Create Wekan Group: ", group)
        pass

