from app.authentik.models import AuthentikUser
from pytest_mock import MockerFixture
from .event_controller import Authentik_Hook_Model, EventController
import pytest

@pytest.fixture()
def mock_user():
    return AuthentikUser(pk="5", username="asd", name="asd", email="asd@example.org")


def test_handle_model_created_event(mocker: MockerFixture, mock_user: AuthentikUser):
    wekan_mock = mocker.MagicMock()
    wekan_mock.get_user.return_value = None
    nextcloud_mock = mocker.MagicMock()
    wekan_mock.get_user.return_value = None
    authentik_mock = mocker.MagicMock()
    authentik_mock.get_user_by_pk.return_value = mock_user

    model = Authentik_Hook_Model(pk=mock_user.pk, app="authentik_core", name=mock_user.name, model_name="user")
    
    ec = EventController(authentik_mock)
    ec.register_api(authentik_mock, [wekan_mock, nextcloud_mock])
    ec.handle_model_created_event(model)
    ec._authentik.get_user_by_pk.assert_called()
    ec._authentik.get_user_by_pk.assert_called_with("5")

    wekan_mock.create_user.assert_called()
    wekan_mock.create_user.assert_called_with(mock_user)
    
    nextcloud_mock.create_user.assert_called()
    nextcloud_mock.create_user.assert_called_with(mock_user)