from fastapi.testclient import TestClient
import pytest

from app.consumer.wekan.api import WekanApi

from .main import Authentik_Hook_Model, app

client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World"}

def test_hook_fails_for_wrong_input():
    d = """{ 
        "body": "Test Notification from transport hook",
        "severity": "notice",
        "user_email": "root@localhost",
        "user_username": "akadmin"
    }"""
    response = client.post("/authentik/hook/", data=d)
    assert response.status_code == 422

def test_hook_model_created(mocker):
    mock = mocker.patch("app.event_controller.EventController.handle_model_created_event")
    d = """
    {"model": {"pk": 5, "app": "authentik_core", "name": "asd", "model_name": "user"}, "http_request": {"args": {}, "path": "/api/v3/core/users/", "method": "POST"}}
    """
    response = client.post("/authentik/hook/", data=d, )
    assert response.status_code == 200
    assert len(mock.mock_calls) > 0
    kall = mock.call_args
    assert type(kall.args[0]) == Authentik_Hook_Model 
    model: Authentik_Hook_Model = kall.args[0]
    assert model.pk == "5"
    assert model.app == "authentik_core"
    assert model.name == "asd"
    assert model.model_name == "user"