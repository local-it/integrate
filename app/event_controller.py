import structlog
from typing import List
from pydantic import BaseModel
from fastapi import FastAPI, Depends
from app.authentik.api import Authentik
from app.authentik.models import AuthentikUser
from app.consumer.baseConsumer import Consumer
from app.consumer.wekan.api import WekanApi
from app.consumer.wekan.main import WekanConsumer
from app.authentik.settings import AuthentikSettings

logging = structlog.get_logger()

class Authentik_Hook_Model(BaseModel):
    pk: str
    app: str
    name: str
    model_name: str


class Http_request(BaseModel):
    args: dict
    path: str
    method: str


authentikSettings = AuthentikSettings()

class EventController:

    def __init__(self, authentik_api: Authentik):
        # try:
        self._authentik = authentik_api
        self._sinks = []
        self._exceptions = []
        for sc in Consumer.__subclasses__():
            obj = sc()
            self._sinks.append(obj)

            
        # except Exception as e:
        #     raise Exception("Failed to init Api", e)
        self.jobs = []
        pass

    def register_api(self, authentik: Authentik, sinks: List[Consumer]):
        self._authentik = authentik
        self._sinks = sinks

    def handle_model_created_event(self, model: Authentik_Hook_Model):
        user: AuthentikUser = self._authentik.get_user_by_pk(model.pk)
        for sink in self._sinks: # TODO this could run async
            logging.info(f"Creating User {user.username} in {sink.__class__}")
            try:
                sink.create_user(user)
            except Exception as e:
                logging.error("create user", exception=str(e), sink=sink, user=user)
                self._exceptions.append(str(e))
        return True
