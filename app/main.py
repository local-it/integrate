import structlog
from unicodedata import name
from urllib.error import HTTPError
from fastapi import Depends, FastAPI, Request, BackgroundTasks
from pydantic import BaseModel
from app.consumer.baseConsumer import BaseUser, Consumer
from app.authentik.api import Authentik
from app.authentik.models import AuthentikUser
from app.event_controller import Authentik_Hook_Model, EventController, Http_request
from app.authentik.settings import AuthentikSettings
from .consumer.wekan.api import WekanApi
from .routers import identityProvider
import json

logging = structlog.get_logger()

app = FastAPI()

app.include_router(identityProvider.router)

@app.get("/")
async def root():
    return {'message': 'Hello World'}

@app.get("/consumer/")
async def get_consumer():
    l = []
    for sc in Consumer.__subclasses__():
        l.append(sc.__name__)
    return l

    
### for testing purposes
@app.get("/authentik/create_hook/")
async def hook(request: Request):
    a = Authentik(base="http://localhost:9000/", token="foobar123")
    res = a.create_web_hook(hook_endpoint="http://172.17.0.1:8000/authentik/hook/") # docker localhost
    logging.info(res)

@app.get("/authentik/users/create_demo_user/")
async def create_demo_user(request: Request):
    a = Authentik(base="http://localhost:9000/", token="foobar123")
    try:
        user = a.create_user(
            AuthentikUser(username="demo", name="dmeo", email="foo@example.org"))
    except Exception as e:  # TODO
        return e
    logging.info(user)
    return user.dict
